package main

import (
	commonpb "git.begroup.team/platform-core/be-central-proto/common"
	"git.begroup.team/platform-transport/f3/config"
	"git.begroup.team/platform-transport/f3/internal/services"
)

func registerBaseService(cfg *config.Config) commonpb.BaseServer {
	return services.NewBase(cfg)
}
