package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/f3/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)
// TODO todo 1
type F3Client struct {
	pb.F3Client
}

func NewF3Client(address string) *F3Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial F3 service", l.Error(err))
	}

	c := pb.NewF3Client(conn)

	return &F3Client{c}
}
