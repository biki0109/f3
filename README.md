# :honeybee:kit

bekit is designed to create a fully functional microservices scaffolding in seconds, allow engineers to focus on the business logic straight away!

The more automation steps and standards, the less confusing arguments and human errors.


# Prerequisites

- Go: https://golang.org/doc/install
- Protoc: https://github.com/golang/protobuf

# Table of contents

 - [Installation](#installation)
 - [Usage](#usage)
 - [Motto](#motto)
 - [TODO list](#todo-list)
 - [Bootstrap a service](#bootstrap-a-service)
 - [Service implementation](#service-implementation)
 - [Model generation](#model-generation)
 - [Model mapping](#model-mapping)
 - [Contributing](#contributing)

 # Installation

 Before you install please read [Prerequisities](#Prerequisites)

 ```
 go get git.begroup.team/platform-core/be-kit
 make depend
 ```

 ## Upgrading
 
 
 ```
 go get -u git.begroup.team/platform-core/be-kit
 ```
 # Motto
 - :wrench: Convention over configuration.
 - :computer: What can be generated, make it generated.
 - :heavy_check_mark: Simple, runnable, get it done.
 - :x: Stay away of network concerns as much as possible.

 # TODO list
 - [x] Support bootstrap, one click new microservice
 - [x] Support model generation
 - [x] Support store generation (~Now using kallax~) (Now using gorm)
 - [x] Support service boilerplate code implementation
 - [x] Support create mapping function between model and proto
 - [ ] More linter and analyze source code
 - [ ] Faster bootstraping by using go routine 
 - [ ] Support prometheus
 - [ ] Support flag grpc-only (negative means grpc gateway integrated)
 
 # Usage

 ```
 $ be-kit help
 
 Be-Kit is designed to create a fully functional microservices scaffolding in seconds, allow engineers to focus on the business logic straight away!

Usage:
  be-kit [command]

Available Commands:
  bootstrap
  help        Help about any command
  models      Manage models, including add and gen
  services    Manage services, including implement
  proto       Manage proto, including create-mapping

Flags:
  -h, --help   help for be-kit

Use "be-kit [command] --help" for more information about a command.
 ```
 
 Be-kit has a small convention: all file auto generated will have suffix of `_gen.go`. That means it does not allow to edit and can be overridden anytime.
 
 Sometimes you also find some special files that have comment of `AUTO-GENERATED BUT EDITTABLE`. In this file be-kit does generate but has some context awareness that make you are able to edit.


 # Bootstrap a service

 ```
 be-kit bootstrap create --name=paygate
 be-kit bootstrap create --name=paygate --db=postgres
 ```

 This command will create a bootstrap of service named paygate, including:
 - Create configurations (With flags set, for example database is postgres, redis ...)
 - Create proto
 - Create Makefile
 - Installing dep
 - Ensure dep
 - Generate protobuf
 - Create service
 - Create cmd
 - Create store
 - Create model

 ```
.
├── ./Dockerfile
├── ./Gopkg.lock
├── ./Gopkg.toml
├── ./Makefile
├── ./cmd
│   ├── ./cmd/client
│   │   └── ./cmd/client/main.go
│   └── ./cmd/server
│       ├── ./cmd/server/main.go
│       ├── ./cmd/server/register_gen.go
│       └── ./cmd/server/server.go
├── ./config
│   ├── ./config/config.go
│   └── ./config/config_gen.go
├── ./descriptors.protoset
├── ./docs
│   └── ./docs/service.swagger.json
├── ./internal
│   ├── ./internal/models
│   │   └── ./internal/models/models.go
│   ├── ./internal/services
│   │   └── ./internal/services/services.go
│   └── ./internal/stores
│       └── ./internal/stores/main.go
├── ./pb
│   ├── ./pb/service.pb.go
│   ├── ./pb/service.pb.gw.go
│   └── ./pb/service.validator.pb.go
└── ./proto
    └── ./proto/service.proto
```

:warning: Now remember to jump into new service to make further commands working by `cd paygate` :warning:

# Service implementation

**Service internally we all use GRPC as the communication protocol.**

Let's say if we want to create new RPC of creating user, the typical way:

:one: We go to `proto/service.proto` and define our function.

We use the convention for commenting. `// service: X` means this function belongs to service X.

```
service Service {
    // service: User
    rpc CreateUser(CreateUserRequest) returns (CreateUserResponse);
}

message CreateUserRequest {
    string id = 1;
    string username = 2;
    string password = 3;
}

message CreateUserResponse {
    string id = 1;
    string username = 2;
}
```

:two: Now we will generate the protobuf by simply run

```
make generate
```

That will re-generate inside `pb/service.pb.go`, we have new RPC function under the service interface

```
type ServiceServer interface {
	// service: User
	CreateUser(context.Context, *CreateUserRequest) (*CreateUserResponse, error)
}
```

:three: Next step we will want to implement that RPC function, just type

```
be-kit services implement

```
bekit will :fire: create if not exist file `internal/services/user.go` and `internal/services/user_gen.go` and generate the boiler plate code that implements the interface, such as

```
func (s *service) CreateUser(context context.Context, req *pb.CreateUserRequest) (*pb.CreateUserResponse, error) {
	return s.createUser(context, req)
}
```

and you are allowed to edit your functionality here. 

```
func (s *service) createUser(context context.Context, req *pb.CreateUserRequest) (*pb.CreateUserResponse, error) {
	return &pb.CreateUserResponse{}, nil
}
```


# Model generation

As the data layer, we also have a model `User` that plays a part as database presenter.

bekit also support functionalities that make you easier in model manipulation.

## Add new model
```
be-kit models add user --suffix=AB
```
This will generate a model `user`, stored in `internal/models/user.go`

The default model contains
- ID 
- CreatedAt, UpdatedAt, DeletedAt

```
type User struct {
	ID string

	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
```

Our ID has format base54 contains 20 chars included:
- 1 env
- 8 timestamp
- 8 entropy
- 1 checksum
- 2 suffix

Each model has unique suffix, that's why you have to provide it before you create a model.

Be-kit also provides to you the ID generator, just call NewID().

## Remove model
```
be-kit models remove user
```
This will remove model named `user`, delete files that are associated to this one and unregister it from `internal/models/register_gen.go` (see [Generate model and store](#generate-model-and-store))
Note that this command is used only for models that you create by calling `be-kit models add` command.

## Import models from dbdiagram
Most of time, you start a project by designing your database on [dbdiagram](https://dbdiagram.io). It would be frustrating to manually transfer each table on your design into your code, especially when you got dozens of tables which in turn have dozens of columns.
Be-kit supports automatically generating concreted models using IDL code you wrote on [dbdiagram](https://dbdiagram.io).
```
be-kit models import -u https://link/to/your/design
```
The above command will also download your IDL code and store it in your repo for logging.

Be-kit also supports importing offline using code file copied from your design.
```
be-kit models import -f path/to/your/file
```
Take this snippet of code for example
```
Table users {
  id int [primary key]
  full_name varchar
  email varchar [unique]
  gender varchar
  date_of_birth varchar
  created_at varchar
  country_code int [ref: > countries.code]
}
```
Running `import` command creates file `user.go` as follow:
```
package models

type Users struct {
	ID          uint32 `json:"id" gorm:"primary_key:true"`
	FullName    string `json:"full_name"`
	Email       string `json:"email" gorm:"unique"`
	Gender      string `json:"gender"`
	DateOfBirth string `json:"date_of_birth"`
	CreatedAt   string `json:"created_at"`
	CountryCode uint32 `json:"country_code"`
}
```
You can use `-e` to exclude the models which you haven't wanted to import yet. Type `be-kit models import -h` for more details.

> The ONLY thing that is CONSTANT is CHANGE.
>
There is a chance that you have to modify your design many times. What about the code you added into your models (like `json`, `xml`, `proto`,... tags or your custom functions)? You fear that they will be cleared if you run import again.
The great part is that be-kit won't touch your custom code. Each time you run `be-kit models import`, it will:
- Remove fields or models that you get rid of from your design.
- Add new fields if exist.
- Modify `gorm` tag if you change your columns' properties (like `unique`, default value).
- Change type of a field if it's corresponding column's type is changed.

So feel free to run `import` on your daily basics (but think twice when you decide to remove a table or column in your design).

:warning: Models generation need the information stored in `.be-kit` folder to work correctly so please commit this folder.   
## Generate model and store

Under the heart of database manipulation is [Gorm](https://github.com/jinzhu/gorm).

To gen
```
be-kit models gen user
```

It will create 2 files generated under `internal/stores/user.go` and `internal/stores/user_gen.go`.

Like services, you are about to edit the `user.go`. `user_gen.go` contains some CRUD functionalities default.

The interesting part is, bekit can understand the source code, it automagically dependency inject the store from the air, which is usually forgotten by the developers :runner::runner::runner:

```
type service struct {
	userStore *stores.UserStore
}

func New(config *config.Config, userStore *stores.UserStore) pb.ServiceServer {
	return &service{
		userStore: userStore}
}

```

Dependency injection also happen when register the service, take a look on `cmd/server/register_gen.go`

**UPDATE**: Now when you run `be-kit models add` or `be-kit models import` command, be-kit will automatically generate model and store.

# Model mapping

One of the most painful and flaky jobs when working with protobuf is you have to write your own mapping functions, which are:
- Transform protobuf binaries from the RPC payload to data presenter (model)
- Transform from data presenter (model) back to RPC response, return to client

When you are in agile development, changing, add on field into RPC functions are likely happen all the time.

Imagine you are having thousand fields belong multiple models and you forget to add the transformation for one field.

Enjoy searching the needle in the sea :skull::skull::skull:

## bekit is your saviour.

bekit is able to understand your protobuf, add your model and creating a mapping function just by adding comment at `internal/models/<your-models-name>.go`
```go
package models

//proto:User
type User struct {}
```
After that, you can run:
```
be-kit proto create-mapping
```

Those generated functions will be stay at `internal/models/gen.go`

```
func FromUserProto(p *pb.User) *User {
	var m User
		m.Password = p.Password
		copy(m.ID[:], p.Id)
		m.Username = p.Username

  	fromUserProto(&m, p)
  	return &m
}

func ToUserProto(m *User) *pb.User {
	p := &pb.UserDB{
		Id:       m.ID[:],
		Username: m.Username,
		Password: m.Password,
	}

	toUserProto(m, &p)
	return &p
}
```
Be-kit will add two function `fromUserProto` and `toUserProto` into `internal/models/custom.go`. Please add your custom modification in this file.

## proto tags
- When you have some fields that you want to ignore from mapping (like custom fields), there is tag supported: `proto:"-"`.
- When you want to map a model field to and from protobuf field which has different name, use the tag `proto:nameOfThisField`.
- If you want to use different names for _to_ and _from_ protobuf field, there are two tag `prototo:nameOfField` and `protofrom:nameOfField` for generating _to_ and _from_ protobuf respectively.

## type conversion
Be-kit supports automatically converting type of a model field to type of a protobuf field (for example `unit32` to `int64`) and vice versa. You no longer have to manually convert datetime to unix timestamp (or `time.Time` to `int64`) cause be-kit also supports time conversion.
> Great power comes great responsibility
>
Please make sure that any type conversion is originated from your intention not your mistake.
# Contributing

* Create your feature branch (for example to faster boostraping):

```
$ git checkout -b feature/refactor-bootstrap
```

* Commit your changes:

```
$ git commit -am "Make bootstrap 10x faster"
```

* Push to the branch:

```
$ git push origin feature/refactor-bootstrap
```

* Submit your pull request


# Contributors

Say thanks to Thach Le, Quang Cao and Trang Mai.

_Let bekit understand you_




